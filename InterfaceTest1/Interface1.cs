﻿using System;

namespace InterfaceTest1
{

    interface IMovable
    {
        public const int minSpeed = 0;
        private static int maxSpeed = 60;
        static int MaxSpeed { get { return maxSpeed; } set { if (value > 0) maxSpeed = value; } }

        static double GetTime(double distance, double speed) => distance / speed;

    }


    class Interface1
    {
        static void Main(string[] args)
        {
            Console.WriteLine(IMovable.MaxSpeed);
            IMovable.MaxSpeed = 65;
            Console.WriteLine(IMovable.MaxSpeed);
            double time = IMovable.GetTime(100, 10);
            Console.WriteLine(time);
        }
    }
}
