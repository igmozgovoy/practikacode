﻿using System;

namespace Interface3
{
    interface IAccount
    {
        int Value { get { return Value; } set { Value = value; } }
        int CurrentSum { get; } // Текущая сумма
        void Put(int sum); // Положить на счет

        void Withdraw(int sum); // Взять со счета
    }

    interface IClien
    {
        string Name { get; set; }
    }

    class Client : IAccount, IClien
    {
        private int _sum; // переменная для хранения суммы
        public string Name { get; set; }
        public Client(string name, int sum)
        {
            Name = name;
            _sum = sum;
        }
        public int CurrentSum { get { return _sum; } }
        public void Put(int sum) { _sum += sum; }
        public void Withdraw(int sum)
        {
            if (_sum >= sum) _sum -= sum;
        }
    }

    class Interface3
    {
        // Множественная реализация интерфейсов
        static void Main(string[] args)
        {
            Client client = new Client("Tom", 200);
            client.Put(300);
            Console.WriteLine(client.CurrentSum);
            client.Withdraw(100);
            Console.WriteLine(client.CurrentSum);

            Console.WriteLine("*********************");

        }
    }
}
