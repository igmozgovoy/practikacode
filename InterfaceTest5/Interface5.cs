﻿using System;

namespace InterfaceTest5
{
    interface IAction
    {
        void Move()
        {
            Console.WriteLine("Move in iAction");
        }

        void Stop()
        {
            Console.WriteLine("Stop");
        }
    }

    // Переопределение методов
    //class BaseAction : IAction
    //{
    //    public virtual void Move()
    //    {
    //        Console.WriteLine("Move in BaseAction");
    //    }
    //}

    //sealed class HeroAction : BaseAction, IAction
    //{
    //    public override void Move()
    //    {
    //        Console.WriteLine("Move in HeroAction");
    //    }
    //}

    // Сокрытие метода
    //class BaseAction : IAction
    //{
    //    public void Move()
    //    {
    //        Console.WriteLine("Move in BaseAction");
    //    }
    //}

    //sealed class HeroAction : BaseAction
    //{ 
    //    public new void Move()
    //    {
    //        Console.WriteLine("Move in HeroAction");
    //    }
    //}

    // Повторная реализации интерфейса в классе-наследние
    //class BaseAction : IAction
    //{
    //    public void Move()
    //    {
    //        Console.WriteLine("Move in BaseAction");
    //    }
    //}

    //sealed class HeroAction : BaseAction, IAction
    //{
    //    public new void Move()
    //    {
    //        Console.WriteLine("Move in HeroAction");
    //    }
    //}

    // явная реализация
    class BaseAction : IAction
    {
        public void Move()
        {
            Console.WriteLine("Move in BaseAction");
        }
    }

    sealed class HeroAction : BaseAction, IAction
    {
        public new void Move()
        {
            Console.WriteLine("Move in HeroAction");
        }

        void IAction.Move()
        {
            Console.WriteLine("Move in Action");
        }
    }

    class Interface5
    {
        // Реализация интерфейсов в базовых и производных классах
        static void Main(string[] args)
        {
            //IAction ob1 = new HeroAction();
            //ob1.Move();

            //HeroAction ob2 = new HeroAction();
            //ob2.Move();

            //IAction ob3 = new BaseAction();
            //ob3.Move();
            //***************************

            //IAction ob1 = new HeroAction();
            //ob1.Move();
            //ob1.Stop();

            //BaseAction ob2 = new HeroAction();
            //ob2.Move();
            ////ob2.Stop(); Ошибка!!
            //((IAction)ob2).Stop();

            //HeroAction ob3 = new HeroAction();
            //ob3.Move();

            //BaseAction ob4 = new BaseAction();
            //ob4.Move();
            //***************************

            //IAction ob1 = new HeroAction();
            //ob1.Move();

            //HeroAction ob2 = new HeroAction();
            //ob2.Move();

            //BaseAction ob3 = new BaseAction();
            //ob3.Move();
            //***************************

            BaseAction ob1 = new HeroAction();
            ob1.Move();

            IAction ob2 = new HeroAction();
            ob2.Move();

            HeroAction ob3 = new HeroAction();
            ob3.Move();

            ((IAction)ob3).Stop();

        }
    }
}
