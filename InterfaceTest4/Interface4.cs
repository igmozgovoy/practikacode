﻿using System;

namespace InterfaceTest4
{
    interface ISchool
    {
        void Study();     
    }
    interface IUniversity
    {
        void Study();
    }

    class Student : ISchool, IUniversity
    {
        void ISchool.Study()
        {
            Console.WriteLine("School");
        }

        void IUniversity.Study()
        {
            Console.WriteLine("University");
        }
    }

    // Явная реализация
    class Interface4
    {
        static void Main(string[] args)
        {
            Student st = new Student();

            ((IUniversity)st).Study();
            ((ISchool)st).Study();
        }
    }
}
