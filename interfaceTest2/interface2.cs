﻿using System;
using System.Diagnostics;

namespace interfaceTest2
{
    interface IMovable
    {
        void Move() 
        {
            Console.WriteLine("Walking");
        }
    }

    internal class Person : IMovable
    {
        
    }

    struct Car : IMovable
    {
        public void Move()
        {
            Console.WriteLine("Driving");
        }
    }

    class interface2
    {
        // Реализация по умолчанию
        static void Main(string[] args)
        {
            IMovable tom = new Person();
            Car tesla = new Car();
            tom.Move();
            tesla.Move();

            // Ошибка: в классе Person не определен метод Move();
            //Person Mike = new Person();
            //Mike.Move();

            Person per = new Person();
            ((IMovable)per).Move();

        }
    }
}
